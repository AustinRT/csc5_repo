/* 
 * File:   main.cpp
 * Author: Art
 *
 * Created on September 7, 2014, 6:29 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

//Question 1
string name;

cout << "Hello, my name is Hal!" << endl;
cout << "What is ypur name?" << endl;
cin >> name;
cout << "Hello, " << name << ". I am glad to meet you!" << endl;

//Question 2
int a = 5;
int b = 10;
int temp;

temp = a;

cout << "a = " << a << " " << "b = " << b << endl;
a = b;
a = temp;
cout << "a = " << b << " " << "b = " << temp << endl;

//Question 3
double meters, miles, feet, inches;

cout << "Enter the number of meters:";
cin >> meters;

miles = meters / 1609.34;
feet = meters * 3.381;
inches = feet * 3.281;

cout << "Miles: " << miles << endl;
cout << "Feet: " << feet << endl;
cout << "Inches: " << inches << endl;

//Question 4
int x;

cout << "Enter a number:";
cin >> x;
x = x +5;
cout << "Result: " << x << endl;

//Question 5a
double score1, score2, score3, avg;

cout << "Enter the first score:";
cin >> score1;
cout << "Enter the second score:";
cin  >> score2;
cout << "Enter the third score:";
cin >> score3;

avg = (score1 + score2 + score3) / 3;

cout << "Average test score: " << avg << endl;

//Question 5b
double score1a, score2a, score3a, avg2;

cout << "Enter the test scores:";
cin >> score1a >> score2a >> score3a;

avg2 = (score1a + score2a + score3a) / 3;

cout << "Average test score: " << avg2 << endl;

//Question 6
string num2;
cout << "Enter a number between 1 and 100:";
cin >> num2;

cout << num2.substr(0,1) << " " << num2.substr(1,1) << " " << num2.substr(2,1)
     << " " << num2.substr(3,1);


    
    return 0;
}