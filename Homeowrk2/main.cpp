/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 16, 2014, 11:41 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

// Qustion 3
string name1, name2, name3;
double quiz1a, quiz2a, quiz3a, quiz4a;
double quiz1b, quiz2b, quiz3b, quiz4b;
double quiz1c, quiz2c, quiz3c, quiz4c;
double avg1, avg2, avg3, avg4;

cout << "Enter the sudent's name:";
cin >> name1;
cout << "Enter the quiz scoees:";
cin >> quiz1a;
cin >> quiz2a;
cin >> quiz3a;
cin >> quiz4a;

cout << "Enter the sudent's name:";
cin >> name2;
cout << "Enter the quiz scoees:";
cin >> quiz1b;
cin >> quiz2b;
cin >> quiz3b;
cin >> quiz4b;

cout << "Enter the sudent's name:";
cin >> name3;
cout << "Enter the quiz scoees:";
cin >> quiz1c;
cin >> quiz2c;
cin >> quiz3c;
cin >> quiz4c;

avg1 = ( quiz1a + quiz1b + quiz1c) / 3;
avg2 = ( quiz2a + quiz2b + quiz2b ) / 3;
avg3 = ( quiz3a + quiz3b + quiz3c ) / 3;
avg4 = ( quiz4a + quiz4b + quiz4b ) / 3;

cout << "name        Quiz 1  Quiz 2  Quiz 3  Quiz 4" << endl;
cout << "----        ------  ------  ------  ------" << endl;
cout << setw(7) << left << name1 << "     " << setw(5) << right 
     << quiz1a << "    " << setw(5) << right << quiz2a << "    "  
     << setw(5) << right << quiz3a << "    " << setw(5) << right 
     << quiz4a << endl;
cout << setw(7) << left << name2 << "     " << setw(5) << right 
     << quiz1b << "    " << setw(5) << right << quiz2b << "    "  
     << setw(5) << right << quiz3b << "    " << setw(5) << right 
     << quiz4b << endl;
cout << setw(7) << left << name3 << "     " << setw(5) << right 
     << quiz1c << "    " << setw(5) << right << quiz2c << "    "  
     << setw(5) << right << quiz3c << "    " << setw(5) << right 
     << quiz4c << endl;
cout << "Average:    " << setprecision(2) << fixed << right << setw(5) << avg1
     << "    " << setprecision(2) << fixed << right << setw(5) << avg2 << "    " 
     << setprecision(2) << fixed << right << setw(5) << avg3 << "    " 
     << setprecision(2) << fixed << right << setw(5) << avg4 << endl;

// Question 4
string aName, aName2, food, adj, color, animal;
int aNum;

cout << "Enter a name: ";
cin >> aName;
cout << "Enter another name: ";
cin >> aName2;
cout << "Enter a number from 100 to 130: ";
cin >> aNum;
cout << "Enter a food: ";
cin >> food;
cout << "Enter an adjuative: ";
cin >> adj;
cout << "Enter a color: ";
cin >> color;
cout << "Enter an animal:";
cin >> animal;

cout << "Dear " << aName << "," <<endl;
cout << endl;
cout << endl;
cout << "I am sorry that I am unable to turn in my homework at this time. " 
     << "First, I ate a rotten " << food << ", which made me turn " << color 
     << " and extremely ill. I came down with a fever of " << aNum 
     << ". Next, my " << adj << " pet " << animal << " must have smelled " 
     << "the remains of the " << food << " on my homework because he ate it. "
     << "I am currently rewriting my homework and hope you will accept it "
     << "late." << endl;
cout << endl;
cout << endl;
cout << "Sincerely," << endl;
cout << aName2 << endl;


    return 0;
}