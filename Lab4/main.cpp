/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 18, 2014, 11:37 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

//Question 1
int grade;

cout << "Enter a grade: ";
cin >> grade;
if (grade < 60)
{
    cout << "F";
}
else if (grade < 70)
{
    cout << "D";
}
else if (grade < 80)
{
    cout << "C";
}
else if (grade < 90)
{
    cout << "B";
}
else cout << "A";
cout << endl;

//Question 2a
int x = 0;
while (x < 10)
{
cout << x << endl;
x = x + 2;
}

//Question 2b
int x1 = 0;
while (x1 < 10)
{
cout << "Enter a grade: ";
cin >> grade;
if (grade < 60)
{
    cout << "F" << endl;
}
else if (grade < 70)
{
    cout << "D" << endl;
}
else if (grade < 80)
{
    cout << "C" << endl;
}
else if (grade < 90)
{
    cout << "B" << endl;
}
else cout << "A" << endl;
x1++;
}

//Queston 3
for (int i = 0; i < 10; i++)
 {
      cout << "Enter a number";
      int num;
      cin >> num;
      cout << num << endl;
 }

//Question4
string letterGrade;

cout << "Enter a letter grade: ";
cin >> letterGrade;
if (letterGrade == "A+")
{
cout << "100+";
}
else if (letterGrade == "A")
{
cout << "100 - 93";
}
else if (letterGrade == "A-")
{
cout << "90 - 92.9";
}
else if (letterGrade == "B+")
{
cout << "87 - 89.9";
}
else if (letterGrade == "B")
{
cout << "83 - 86.9";
}
else if (letterGrade == "B-")
{
cout << "80 - 82.9";
}
else if (letterGrade == "C+")
{
cout << "77 - 79.9";
}
else if (letterGrade == "C")
{
cout << "73 - 76.9";
}
else if (letterGrade == "C-")
{
cout << "70 - 72.9";
}
else cout << "Error";



    return 0;
}