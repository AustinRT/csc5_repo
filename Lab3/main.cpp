#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
*
*/
int main(int argh, char**argv){

//Question 1
string val1 = "1";
int val2 = 2;

cout << val1 << " + " << val1 << " = " << val1 + val1 << endl;
cout << val2 << " + " << val2 << " = " << val2 + val2 << endl;

//Question 2
double singles, doubles, triples, homeRuns, atBat, avg;

cout << "Enter the number of singles:";
cin >> singles;
cout << "Enter the number of doubles:";
cin >> doubles;
cout << "Enter the number of triples:";
cin >> triples;
cout << "Enter the number of home runs:";
cin >> homeRuns;
cout << "Enter how many times at bat:";
cin >> atBat;

avg = (singles + ( doubles * 2 ) + ( triples * 3 ) + ( homeRuns * 4 )) / atBat;

cout << setprecision( 2  ) << avg << endl;

//Question 3
int x = 6;
int y = 190;
int temp = x;

cout << "x = " << setw(4) << left << x << " y = " << setw(4) << left 
        << y << endl;
cout << "-----------------------------------------------------" << endl;
cout << "x = " << setw(4) << left << y << " y = " << setw(2) << left 
        << temp << endl;

//Question 4
int x1 = 7;
int y1 = 11;

cout << x1 << " " << y1 <<endl;

if (x1 < 6)
{
    y1 = 11;
}
else if (x1 > 6)
{
    y1 = 5;
}
else y1 = 15;

cout << x1 << " " << y1 <<endl;

//Question 5


    return 0;
}