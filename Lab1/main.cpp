/* 
 * File:   main.cpp
 * Author: Art
 *
 * Created on September 26, 2014, 9:51 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl;
    string name;
    cin >> name;
    cout << "Hello, " << name << ". I am glad to meet you.";

    return 0;
}

