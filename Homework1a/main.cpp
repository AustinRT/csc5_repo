/* 
 * File:   main.cpp
 * Author: Art
 *
 * Created on September 10, 2014, 7:47 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

//Questions 5
int number_of_pods, peas_per_pod, total_peas, product_of_peas;

cout << "Hello\n";
cout << "Press return after endering a number.\n";
cout << "Enter the number of pods:\n";

cin >> number_of_pods;

cout << "Enter the number of peas in a pod:\n";
cin >> peas_per_pod;
total_peas = number_of_pods + peas_per_pod;
product_of_peas = number_of_pods * peas_per_pod;
cout << "If you have ";
cout << number_of_pods;
cout << " pea pors\n";
cout << "and ";
cout << peas_per_pod;
cout << " peas in each pod, then\n";
cout << "you have a sum of ";
cout << total_peas;
cout << "and the product of ";
cout << product_of_peas;
cout << " peas in all the pods.\n";
cout << "Good-buy\n";
cout << "This is the end of the program\n";

//Question 7
cout << "*******************************************\n";
cout << " \n";
cout << "            CCC            SSSS       !!\n";
cout << "           C   C          S    S      !!\n";
cout << "          C     C        S            !!\n";
cout << "         C                S           !!\n";
cout << "         C                 SSSS       !!\n";
cout << "         C                     S      !!\n";
cout << "          C                     S     !!\n";
cout << "           C    C          S    S       \n";
cout << "            CCCC            SSSS      00\n";
cout << " \n";
cout << "*******************************************\n";
    return 0;
}

